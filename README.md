update_addons
=============

CLI for installing and updating WoW addons.

    usage: update_addons [-h] {list,update,install,remove} ...

    Actions:
        list:       This simply lists all installed addons. No other options are
                    required or accepted.

        update:     This will update existing addons.  There is an optional name
                    argument (--name or -n) where you can provide the name of a
                    specific addon to update.  If it is provided then only that
                    addon will be updated.  If it is left off then all installed
                    addons will be updated.

        install:    This will install a new addon.  It has one required argument
                    to specify the name of the addon to install (--name or -n),
                    and has two possible sub-actions:

                    default:    This will install an addon where you must provide
                                the URL to get the version (--version_url or -v)
                                and the URL of the zip file (--download_url or -d).
                                If the version number is part of the file you can
                                specify its location with {version} inside the URL.
                                Example:  http://addon.com/dl/addon-{version}.zip
                    curse:      This will install an addon from curse.com.  There is
                                one available sub-option, --path or -p, which is the
                                last part of the path to the page for this addon on
                                curse.
                                Example: The addon xCT+ is at this location:
                                http://www.curse.com/addons/wow/xct-plus
                                The path you would supply is the last part, xct-plus

        remove:     This will remove a single addon.  It has one required option to
                    provide the name of the addon to remove (--name or -n).
