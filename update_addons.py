#!/usr/bin/env python

import os
import sqlite3

from lxml.html import parse
from re import match
from shutil import rmtree
from urllib2 import urlopen
from cStringIO import StringIO
from zipfile import ZipFile, BadZipfile

WOW_FOLDER = ''
ADDONS_DB = os.path.join(WOW_FOLDER, 'addons.db')


class Addon(object):
    def __init__(self, conn, addons_folder, type, name, **kwargs):
        self.conn = conn
        self.addons_folder = addons_folder
        self.type = type
        self.name = name
        self.id = kwargs.get('id')
        self.folders = kwargs.get('folders')
        self.installed_version = kwargs.get('installed_version')
        self.version_url = kwargs.get('version_url')
        self.download_url = kwargs.get('download_url')
        self.path = kwargs.get('path')

    def save(self):
        c = self.conn.cursor()
        if self.id:
            query = """
                UPDATE addons
                SET    type = :type,
                       name = :name,
                       folders = :folders,
                       installed_version = :installed_version,
                       version_url = :version_url,
                       download_url = :download_url,
                       path = :path
                WHERE  id = :id;"""
        else:
            query = """
                INSERT INTO addons (
                    type,
                    name,
                    folders,
                    installed_version,
                    version_url,
                    download_url,
                    path
                ) VALUES (
                    :type,
                    :name,
                    :folders,
                    :installed_version,
                    :version_url,
                    :download_url,
                    :path
                );"""
        c.execute(query, self.__dict__)
        self.id = c.lastrowid
        self.conn.commit()

    def delete(self):
        if self.id:
            c = self.conn.cursor()
            c.execute("DELETE FROM addons WHERE id = ?", (self.id,))
            self.conn.commit()
        self.id = None
        self.remove_old_version()

    def get_latest_version_and_file(self):
        if self.type == 'curse':
            version = None
            file_location = None

            # Open the page on curse
            url = "http://www.curse.com/addons/wow/{0}".format(
                self.path or self.name.lower().replace(' ', '-'))
            try:
                page = parse(url).getroot()
            except:
                return version, file_location

            # Get version from curse page.
            version_elements = page.find_class('newest-file')
            if len(version_elements) == 1:
                m = match(r'Newest File:\s+(?P<version>.+)',
                          version_elements[0].text_content())
                if m:
                    version = m.group('version')

            # Find download button and get file location from it.
            download_button_em = page.cssselect('em.cta-button.download-large')
            if len(download_button_em) == 1:
                for link in download_button_em[0].iterlinks():
                    url = "http://www.curse.com{0}".format(link[2])
                if url:
                    try:
                        page = parse(url).getroot()
                    except:
                        return version, file_location
                    download_links = page.cssselect('a.download-link')
                    if len(download_links) == 1:
                        file_location = download_links[0].get('data-href')
        elif self.type == 'tukui':
            version = None
            file_location = None
            tukui_url = "http://www.tukui.org/addons/index.php?act={0}&id={1}"
            dl_text = 'Click here if you are not redirected automatically.'

            url = tukui_url.format("view", self.path)
            try:
                page = parse(url).getroot()
            except:
                return version, file_location

            # Get version from tukui dl page.
            cells = page.cssselect('td')
            version_labels = []
            for cell in cells:
                if cell.text and cell.text.find('Version') != -1:
                    version_labels.append(cell)
            if version_labels:
                version_label = version_labels[0]
                if len(cells) > cells.index(version_label):
                    version_cell = cells[cells.index(version_label) + 1]
                    if version_cell is not None:
                        version = version_cell.text

            # Get file location from download redirect page.
            url = tukui_url.format("download", self.path)
            try:
                page = parse(url).getroot()
            except:
                return version, file_location
            links = page.cssselect('a')
            dl_links = []
            for link in links:
                if link.text and link.text == dl_text:
                    dl_links.append(link)
            if dl_links:
                file_location = dl_links[0].get('href')
        else:
            version = urlopen(self.version_url).read()
            file_location = self.download_url.format(version=version)
        return version, file_location

    def remove_old_version(self):
        for folder in self.folders.split(','):
            print("Removing old folder: {0}".format(folder))
            folder = os.path.join(self.addons_folder, folder)
            if os.path.exists(folder):
                rmtree(folder)

    def install_new_version(self, version, url):
        print("Installing version {0} of {1}.".format(version, self.name))
        filename = os.path.basename(url)
        print("Downloading: {0}".format(url))
        try:
            f = StringIO(urlopen(url).read())
        except:
            print("Unable to download file.")
            return
        print("Extracting: {0}".format(filename))
        try:
            zipfile = ZipFile(f)
        except BadZipfile:
            print("File is not a valid zip file.")
            return
        self.folders = ','.join(sorted(set(
            fname.split('/',)[0] for fname in zipfile.namelist())))
        zipfile.extractall(self.addons_folder)
        f.close()
        self.installed_version = version
        self.save()

    def update_addon(self):
        latest_version, file_location = self.get_latest_version_and_file()
        if not self.installed_version:
            print("Installing {0}".format(self.name))
            if latest_version and file_location:
                self.install_new_version(latest_version, file_location)
            else:
                print("Unable to find {0}.".format(self.name))
        elif self.installed_version != latest_version:
            print("Updating: {0}".format(self.name))
            if latest_version and file_location:
                self.remove_old_version()
                self.install_new_version(latest_version, file_location)
            else:
                print("Unable to find {0}. Leaving old version.".format(
                    self.name))
        else:
            print("{0} is already up to date.".format(
                self.name, self.installed_version))


class AddonUpdater(object):
    def __init__(self, addons_db, wow_folder):
        self.conn = self.setup_conn(addons_db)
        self.addons_folder = os.path.join(wow_folder, 'Interface/AddOns')

    def setup_conn(self, addons_db):
        def dict_factory(cursor, row):
            d = {}
            for idx, col in enumerate(cursor.description):
                d[col[0]] = row[idx]
            return d
        conn = sqlite3.connect(addons_db)
        conn.row_factory = dict_factory
        c = conn.cursor()
        c.execute("""
            CREATE TABLE IF NOT EXISTS addons (
                id                  INTEGER PRIMARY KEY AUTOINCREMENT,
                type                TEXT NOT NULL,
                name                TEXT NOT NULL UNIQUE,
                folders             TEXT,
                installed_version   TEXT,
                version_url         TEXT,
                download_url        TEXT,
                path                TEXT
            );""")
        conn.commit()
        return conn

    def get_all_addons(self):
        addons = []
        c = self.conn.cursor()
        c.execute("SELECT * FROM addons ORDER BY name;")
        for addon in c.fetchall():
            addons.append(Addon(self.conn, self.addons_folder, **addon))
        return addons

    def get_addon_by_name(self, name):
        c = self.conn.cursor()
        c.execute("SELECT * FROM addons WHERE name = ? LIMIT 1;", (name,))
        addon = c.fetchone()
        if addon:
            return Addon(self.conn, self.addons_folder, **addon)

    def install_addon(self, type, name, **kwargs):
        if self.get_addon_by_name(name):
            print("{0} is already installed.".format(name))
        else:
            addon = Addon(self.conn, self.addons_folder, type, name, **kwargs)
            latest_version, file_location = addon.get_latest_version_and_file()
            if latest_version and file_location:
                addon.install_new_version(latest_version, file_location)
            else:
                print("Unable to find {0} to install it.".format(name))

    def update_all_addons(self):
        for addon in self.get_all_addons():
            addon.update_addon()

    def update_addon_by_name(self, name):
        addon = self.get_addon_by_name(name)
        addon.update_addon()

    def remove_addon_by_name(self, name):
        addon = self.get_addon_by_name(name)
        if not addon:
            print("{0} is not installed.".format(name))
        confirmation = raw_input("Are you sure? (y/[n])")
        if confirmation == 'y':
            addon.delete()

    def list_installed_addons(self):
        for addon in self.get_all_addons():
            print("{name} ({installed_version})".format(**addon.__dict__))


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        prog='update_addons',
        description='CLI for installing and updating WoW addons.')
    subparsers = parser.add_subparsers(
        title='actions', dest='action',
        help='update_addons action -h for action specific help')

    list_parser = subparsers.add_parser('list', help='list installed addons')

    update_parser = subparsers.add_parser(
        'update', help='update existing addon or all addons')
    update_parser.add_argument(
        '--name', '-n', help='addon name to update, leave off to update all')

    name_parser = argparse.ArgumentParser('name', add_help=False)
    name_parser.add_argument('--name', '-n', required=True,
                             help='addon name to install')

    install_parser = subparsers.add_parser('install',
                                           help='install a new addon')

    install_subparsers = install_parser.add_subparsers(
        title='install type', dest='type', help='specify the type of install: '
        'default, curse or tukui, update_addons install type -h for type '
        'specific help')

    default_parser = install_subparsers.add_parser(
        'default', parents=[name_parser], help='install a default style addon')
    default_parser.add_argument('--version-url', '-v', required=True,
                                help='url to check for latest version')
    default_parser.add_argument('--download-url', '-d', required=True,
                                help='location to download the latest version')

    curse_parser = install_subparsers.add_parser(
        'curse', parents=[name_parser], help='install a curse addon')
    curse_parser.add_argument(
        '--path', '-p', help='if the last part of the Curse URL is not the '
        'addon name in lower case, with spaces replaced by dashes, specify '
        'the last part of the path here.')

    tukui_parser = install_subparsers.add_parser(
        'tukui', parents=[name_parser], help='install a tukui addon')
    tukui_parser.add_argument(
        '--path', '-p', required=True,
        help='The ID for the tukui addon from its URL should go here.')

    remove_parser = subparsers.add_parser('remove',
                                          help='remove an existing addon')
    remove_parser.add_argument('--name', '-n', required=True,
                               help='addon name to remove')

    args = parser.parse_args()

    updater = AddonUpdater(ADDONS_DB, WOW_FOLDER)
    if args.action == 'list':
        updater.list_installed_addons()
    elif args.action == 'update':
        if args.name:
            updater.update_addon_by_name(args.name)
        else:
            updater.update_all_addons()
    elif args.action == 'install':
        if args.type == 'default':
            updater.install_addon(
                args.type, args.name, version_url=args.version_url,
                download_url=args.download_url)
        elif args.type == 'curse':
            updater.install_addon(args.type, args.name, path=args.path)
        elif args.type == 'tukui':
            updater.install_addon(args.type, args.name, path=args.path)
    elif args.action == 'remove':
        updater.remove_addon_by_name(args.name)
